FROM node:14-alpine

# Create folder for app
RUN mkdir -p /home/node-app

# Copy the app dir to the one in the container
COPY ./app /home/node-app

# Set the working directory to the container app folder
WORKDIR /home/node-app

# Install dependencies
RUN npm install

# Expose server port
EXPOSE 3000

# Start server
CMD ["node", "server.js"]