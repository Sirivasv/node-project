#!/usr/bin/env groovy

@Library('jenkins-shared-library')_

pipeline {
  agent any
  stages {
    stage('Increment version') {
      steps {
        script {
          echo "Running npm bump version command"
          def new_version = bumpPatchVersionNode()
          echo "New version: $new_version"
          env.IMAGE_NAME = "sirivasv/demo-repo:nm9-$new_version-$env.BUILD_NUMBER"
          echo "Image name: $env.IMAGE_NAME"
        }
      }
    }
    stage('Running tests') {
      steps {
        script {
          echo "Installing Dependencies"
          installDependenciesNode()
          
          echo "Running Tests"
          runTestsNode()

          echo "Clean node_modules folder"
          cleanNodeModules()
        }
      }
    }
    stage("Build and publish docker image") {
      when {
        expression {
          BRANCH_NAME == 'master'
        }
      }
      steps {
        script {
          echo "Building Docker Image"
          buildImage(env.IMAGE_NAME)

          echo "LogIn In Docker Hub"
          dockerLogin()

          echo "Push Docker Image"
          dockerPush(env.IMAGE_NAME)
          
        }
      }
    }
    stage('Deploy to ec2') {
      when {
        expression {
          BRANCH_NAME == 'master'
        }
      }
      steps {
        script {
          echo "Deploying docker image and Compose to EC2..."
          def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
          def ec2Instance = "ec2-user@52.53.178.41"
          sshagent(['ec2-docker-server']) {
              sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} echo \"setting first connection\""
              sh "scp server-cmds.sh ${ec2Instance}:/home/ec2-user"
              sh "scp docker-compose.yaml ${ec2Instance}:/home/ec2-user"
              sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
          }
        }
      }
    }
    stage('commit version update') {
      when {
        expression {
          BRANCH_NAME == 'master'
        }
      }
      steps {
        script {
          echo "Config git global user"
          configGlobalGitUser()

          echo "Print Git information"
          printStatusGit()

          echo "Set remote origin in Git"
          setRemoteOriginGit("gitlab.com/Sirivasv/node-project.git")

          echo "Git add current Directory changes"
          addDirGit(".")

          echo "Git commit changes"
          commitGit("Version Bump")

          echo "Git push current changes in current branch: ${BRANCH_NAME}"
          pushCurrentGit("${BRANCH_NAME}")
        }
      }
    }
  }
}